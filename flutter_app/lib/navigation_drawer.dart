import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'login.dart';

class NavigationDrawer extends StatefulWidget {
  _NavigationDrawerState createState() => _NavigationDrawerState();
}

class _NavigationDrawerState extends State<NavigationDrawer> {
  String user;
  bool isLoggedIn;

  @override
  void initState() {
    super.initState();

    getUser().then((String value) {
      setState(() {
        user = value;
        isLoggedIn = user == null;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    Widget bottomTile = LoginTile();
    if (_isLoggedIn()) {
      bottomTile = LogoutTile();
    }

    var _listItems = [
      LoginHeader(user: this.user),
      NavigationItem(
        description: "View Films",
        route: "/films/view",
      ),
    ];

    if (_isLoggedIn()) {
      _listItems.add(NavigationItem(
        description: "Add Film",
        route: "/films/add",
      ));
    }

    _listItems.add(bottomTile);

    return Drawer(
      child: ListView(
        children: _listItems,
      ),
    );
  }

  bool _isLoggedIn() {
    return isLoggedIn == null || !isLoggedIn;
  }
}

class LoginHeader extends StatelessWidget {
  final String user;

  LoginHeader({@required this.user});

  @override
  Widget build(BuildContext context) {
    if (this.user == null) {
      return buildHeader("Logged out");
    }
    return buildHeader(this.user);
  }

  DrawerHeader buildHeader(String title) {
    return DrawerHeader(
      child: Text(title),
      decoration: BoxDecoration(color: Colors.blue),
    );
  }
}

class NavigationItem extends StatelessWidget {
  final String description;
  final String route;

  NavigationItem({@required this.description, @required this.route});

  @override
  Widget build(BuildContext context) {
    return ListTile(
        title: Text(this.description),
        onTap: () {
          Navigator.pushReplacementNamed(
            context,
            this.route,
          );
        });
  }
}

class LoginTile extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return NavigationItem(
      description: "Login",
      route: "/login",
    );
  }
}

class LogoutTile extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ListTile(
        title: Text("Logout"),
        onTap: () {
          logout();
          Navigator.of(context).pop();
        });
  }
}
