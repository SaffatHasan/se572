import 'package:flutter/material.dart';
import 'package:flutter_app/widgets/StringFormField.dart';
import 'package:http/http.dart' as http;
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'dart:convert' show json, base64, ascii;
import 'navigation_drawer.dart';
import 'config.dart';
import 'widgets/AlertDialog.dart';

final storage = FlutterSecureStorage();

class LoginPage extends StatelessWidget {
  static String routeName = '/login';
  final _userNameController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Login"),
      ),
      body: Padding(
          padding: EdgeInsets.all(8.0),
          child: Column(
            children: [
              StringFormField(
                label: "Username",
                textController: _userNameController,
              ),
              RaisedButton(
                  child: Text("Login"),
                  onPressed: () async {
                    var user = _userNameController.text;
                    if (user == "") {
                      showErrorString(context, "User cannot be empty.");
                      return;
                    }

                    var jwt = await login(user);

                    // check for success
                    if (jwt == null) {
                      showErrorString(context,
                          "Your guess is as good as mine. I really have no idea what went wrong.");
                      return;
                    }

                    // save jwt
                    var payload;
                    try {
                      payload = extract(jwt);
                    } catch (err) {
                      showErrorString(
                          context, "What 💩 are you putting in my login form?");
                      return;
                    }

                    storage.write(key: "username", value: payload['username']);
                    storage.write(key: "jwt", value: jwt);

                    // Route to home
                    Navigator.pushReplacementNamed(
                      context,
                      Navigator.defaultRouteName,
                    );
                  })
            ],
          )),
      drawer: NavigationDrawer(),
    );
  }

  Future<String> login(String user) async {
    var result = await http.post(getConfig().usersURI, body: {
      "username": user,
    });
    if (result.statusCode == 200) {
      return result.body;
    }
    return null;
  }
}

Future<String> getUser() async {
  return storage.read(key: 'username');
}

Future<String> getJWT() async {
  return storage.read(key: 'jwt');
}

Map<String, dynamic> extract(String jwt) {
  return json
      .decode(ascii.decode(base64.decode(base64.normalize(jwt.split(".")[1]))));
}

logout() async {
  storage.deleteAll();
}
