import 'package:flutter/material.dart';
import 'package:flutter_app/navigation_drawer.dart';
import 'package:graphql_flutter/graphql_flutter.dart';
import 'services/film_client.dart';
import 'navigation_drawer.dart';
import 'FilmForms.dart';

class FilmPage extends StatefulWidget {
  static String routeName = '/films/view';
  FilmPage({Key key}) : super(key: key);

  @override
  _FilmPageState createState() => _FilmPageState();
}

class _FilmPageState extends State<FilmPage> {
  final newTaskController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Query(
        options:
            QueryOptions(documentNode: gql(getFilmsQuery), pollInterval: 1),
        builder: (QueryResult result,
            {VoidCallback refetch, FetchMore fetchMore}) {
          return Scaffold(
            appBar: AppBar(
              title: Text("List of Films"),
            ),
            body: Center(
              child: result.hasException
                  ? Text(result.exception.toString())
                  : result.loading
                      ? CircularProgressIndicator()
                      : FilmTable(
                          films: result.data['films'],
                        ),
            ),
            drawer: NavigationDrawer(),
          );
        });
  }
}

class FilmTable extends StatelessWidget {
  FilmTable({@required this.films});
  final List films;
  @override
  Widget build(BuildContext context) {
    return ConstrainedBox(
      constraints:
          BoxConstraints.expand(width: MediaQuery.of(context).size.width),
      child: FilmDataTable(
        films: this.films,
      ),
    );
  }
}

class FilmDataTable extends StatelessWidget {
  FilmDataTable({@required this.films});
  final List films;

  @override
  Widget build(BuildContext context) {
    const header = <DataColumn>[
      DataColumn(
        label: Text("Name"),
      ),
      DataColumn(
        label: Text("Rating"),
        numeric: true,
      ),
    ];

    if (films == null) {
      return DataTable(
        columns: header,
        showCheckboxColumn: false,
        rows: null,
      );
    }

    var data = films
        .map((film) => DataRow(
              cells: <DataCell>[
                DataCell(
                  Text(film['name']),
                ),
                DataCell(
                  Row(
                    children: [
                      for (var i = 0; i < film['rating']; i++) Icon(Icons.star)
                    ],
                  ),
                ),
              ],
              onSelectChanged: (_) => gotoUpdatePage(context, film),
            ))
        .toList();

    return DataTable(
      columns: header,
      rows: data,
      showCheckboxColumn: false,
    );
  }
}

gotoUpdatePage(BuildContext context, film) {
  Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => UpdateFilmForm(
          id: film['_id'],
          name: film['name'],
          rating: film['rating'],
        ),
      ));
}
