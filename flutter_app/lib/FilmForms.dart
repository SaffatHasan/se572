import 'package:flutter/material.dart';

import 'login.dart';
import 'services/film_client.dart';
import 'navigation.dart';
import 'widgets/AlertDialog.dart';
import 'widgets/NameFormField.dart';
import 'widgets/RatingFormField.dart';
import 'widgets/StandardForm.dart';

class AddFilmForm extends StatelessWidget {
  static String routeName = '/films/add';

  AddFilmForm({this.name, this.rating});
  final String name;
  final int rating;

  @override
  Widget build(BuildContext context) {
    return UpdateFilmForm(
      id: null,
      name: this.name,
      rating: this.rating,
    );
  }
}

class UpdateFilmForm extends StatefulWidget {
  UpdateFilmForm({this.id, this.name, this.rating});

  final String id;
  final String name;
  final int rating;

  @override
  _UpdateFilmFormState createState() => _UpdateFilmFormState();
}

class _UpdateFilmFormState extends State<UpdateFilmForm> {
  final nameController = TextEditingController();
  int selectedRating;

  @override
  initState() {
    super.initState();
    nameController.text = widget.name;
    selectedRating = widget.rating;
  }

  @override
  Widget build(BuildContext context) {
    final nameField = NameFormField(
      textController: nameController,
    );

    final ratingField = RatingFormField(
      onChange: this.setSelectedRating,
      selectedRating: selectedRating,
    );

    final updateSubmitButtom = UpdateFilmButton(
      id: widget.id,
      nameController: nameController,
      rating: selectedRating,
    );

    final addSubmitButton = AddFilmButton(
      nameController: nameController,
      rating: selectedRating,
    );

    // choose submit type at runtime
    final submitButton =
        widget.id == null ? addSubmitButton : updateSubmitButtom;

    final formElements = [nameField, ratingField, submitButton];
    return StandardForm(
      title: "Update a film",
      fields: formElements,
    );
  }

  void setSelectedRating(int newValue) {
    setState(() {
      selectedRating = newValue;
    });
  }
}

class AddFilmButton extends StatelessWidget {
  AddFilmButton({
    @required this.nameController,
    @required this.rating,
  });
  final TextEditingController nameController;
  final int rating;

  @override
  Widget build(BuildContext context) {
    final actionFunction = () async {
      final user = await getUser();
      if (user == null) {
        showErrorString(context, "You must login to perform this action!");
        return;
      }

      if (this.nameController.value.text == "") {
        showErrorString(context, "Film name cannot be empty!");
        return;
      }

      addFilm(
        this.nameController.value.text,
        this.rating,
      ).then(
        (value) => goHome(context),
        onError: (error) => showError(context, error),
      );
    };

    return SubmitButton(
      label: "Add",
      onPressed: actionFunction,
    );
  }
}

class UpdateFilmButton extends StatelessWidget {
  UpdateFilmButton({
    @required this.id,
    @required this.nameController,
    @required this.rating,
  });
  final String id;
  final TextEditingController nameController;
  final int rating;

  @override
  Widget build(BuildContext context) {
    final actionFunction = () async {
      final user = await getUser();
      if (user == null) {
        showErrorString(context, "You must login to perform this action!");
        return;
      }

      if (this.nameController.value.text.isEmpty) {
        showErrorString(context, "Film name cannot be empty!");
        return;
      }
      updateFilm(
        this.id,
        this.nameController.value.text,
        this.rating,
      ).then(
        (value) => goHome(context),
        onError: (error) => showError(context, error),
      );
    };

    return SubmitButton(
      label: "Update",
      onPressed: actionFunction,
    );
  }
}

class SubmitButton extends StatelessWidget {
  SubmitButton({
    @required this.label,
    @required this.onPressed,
  });

  final String label;
  final Function onPressed;

  @override
  Widget build(BuildContext context) {
    return RaisedButton(
      child: Text(this.label),
      onPressed: this.onPressed,
    );
  }
}
