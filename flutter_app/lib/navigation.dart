import 'package:flutter/material.dart';

void goHome(BuildContext context) {
  Navigator.of(context).pushReplacementNamed(Navigator.defaultRouteName);
}
