import 'package:flutter/foundation.dart';
import 'package:graphql_flutter/graphql_flutter.dart';
import '../login.dart';
import '../config.dart';

final authLink = AuthLink(getToken: getJWT);
final httpLink = HttpLink(uri: getConfig().filmsURI);

ValueNotifier<GraphQLClient> unauthenticatedFilmsClient =
    ValueNotifier(GraphQLClient(
  cache: InMemoryCache(),
  link: httpLink,
));

ValueNotifier<GraphQLClient> getAuthenticatedClient() {
  return ValueNotifier(GraphQLClient(
    cache: InMemoryCache(),
    link: authLink.concat(httpLink),
  ));
}

final String getFilmsQuery = """
query {
  films {
    _id,
    name,
    rating,
  }
}
""";

final String addFilmMutation = """
mutation addFilm (\$name: String!, \$rating: Int!) {
  addFilm(name: \$name, rating: \$rating) {
    _id
    name
    rating
  }
}
""";

final String updateFilmMutation = """
mutation updateFilm (\$_id: String!, \$name: String!, \$rating: Int!) {
  updateFilm(_id: \$_id, name: \$name, rating: \$rating) {
    _id
    name
    rating
  }
}
""";

Future<dynamic> addFilm(String name, int rating) async {
  final variables = {
    "name": name,
    "rating": rating,
  };
  return mutation(addFilmMutation, variables);
}

Future<dynamic> updateFilm(String _id, String name, int rating) async {
  final variables = {
    "_id": _id,
    "name": name,
    "rating": rating,
  };

  return mutation(updateFilmMutation, variables);
}

Future<dynamic> mutation(String mutationString, variables) async {
  final response = await getAuthenticatedClient().value.mutate(MutationOptions(
        variables: variables,
        documentNode: gql(mutationString),
      ));
  if (response.hasException) {
    return Future.error(AssertionError(response.exception.toString()));
  }
  return response.data;
}
