// Copyright 2018 The Flutter team. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

import 'package:flutter/material.dart';
import 'package:graphql_flutter/graphql_flutter.dart';
import 'services/film_client.dart';
import 'film_list.dart';
import 'routes.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return GraphQLProvider(
      client: unauthenticatedFilmsClient,
      child: MaterialApp(
        initialRoute: FilmPage.routeName,
        routes: appRoutes,
      ),
    );
  }
}
