import 'package:flutter/material.dart';

class StringFormField extends StatelessWidget {
  StringFormField({@required this.textController, @required this.label});
  final TextEditingController textController;
  final String label;

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      controller: this.textController,
      decoration: InputDecoration(
        contentPadding: EdgeInsets.all(10),
        labelText: this.label,
      ),
      validator: (String arg) {
        if (arg.length == 0) {
          return "Cannot be empty";
        }
        return null;
      },
    );
  }
}
