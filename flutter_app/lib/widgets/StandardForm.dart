import 'package:flutter/material.dart';
import 'package:flutter_app/navigation_drawer.dart';

class StandardForm extends StatelessWidget {
  StandardForm({@required this.title, @required this.fields});

  final String title;
  final List<Widget> fields;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text(this.title)),
      body: Form(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            for (var field in this.fields)
              _CustomPadding(
                child: field,
              )
          ],
        ),
      ),
      drawer: NavigationDrawer(),
    );
  }
}

class _CustomPadding extends StatelessWidget {
  _CustomPadding({@required this.child});
  final child;

  @override
  Widget build(BuildContext context) {
    return Padding(padding: EdgeInsets.all(10), child: this.child);
  }
}
