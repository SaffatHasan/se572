import 'package:flutter/material.dart';

class RatingFormField extends StatelessWidget {
  RatingFormField({@required this.selectedRating, @required this.onChange});
  final int selectedRating;
  final _validRatings = [1, 2, 3, 4, 5];
  final onChange;

  @override
  Widget build(BuildContext context) {
    return DropdownButtonFormField(
        decoration: InputDecoration(
          contentPadding: EdgeInsets.all(10),
          labelText: "Rating",
        ),
        value: this.selectedRating,
        onChanged: this.onChange,
        items: this._validRatings.map<DropdownMenuItem<int>>((int value) {
          return DropdownMenuItem<int>(
            value: value,
            child: Text(value.toString()),
          );
        }).toList());
  }
}
