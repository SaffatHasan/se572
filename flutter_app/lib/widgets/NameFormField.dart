import 'package:flutter/material.dart';
import 'StringFormField.dart';

class NameFormField extends StatelessWidget {
  NameFormField({@required this.textController});
  final TextEditingController textController;

  @override
  Widget build(BuildContext context) {
    return StringFormField(
      label: "Name",
      textController: this.textController,
    );
  }
}
