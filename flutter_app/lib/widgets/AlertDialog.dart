import 'package:flutter/material.dart';

void showError(BuildContext context, Error error) {
  showErrorString(
    context,
    error.toString(),
  );
}

void showErrorString(BuildContext context, String error) {
  displayDialog(
    context,
    "An Error Occurred!",
    error,
  );
}

void displayDialog(BuildContext context, String title, String text) {
  showDialog(
      context: context,
      builder: (context) => AlertDialog(
            title: Text(title),
            content: Text(text),
          ));
}
