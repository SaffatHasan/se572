import 'package:flutter/material.dart';
import 'FilmForms.dart';
import 'film_list.dart';
import 'login.dart';

final Map<String, Widget Function(BuildContext)> appRoutes = {
  // Default page
  Navigator.defaultRouteName: (context) => FilmPage(),

  // Film routes
  FilmPage.routeName: (context) => FilmPage(),
  AddFilmForm.routeName: (context) => AddFilmForm(),

  // Auth routes
  LoginPage.routeName: (context) => LoginPage(),
};
