import 'package:flutter/cupertino.dart';

class AppConfig {
  final String usersURI;
  final String filmsURI;

  AppConfig({@required this.usersURI, @required this.filmsURI});
}

final deployedURI = "http://192.168.1.152";

final localConfig = AppConfig(
  filmsURI: "$deployedURI:3001/graphql",
  usersURI: "$deployedURI:3002/login",
);

final dockerConfig = AppConfig(
  filmsURI: "$deployedURI:8080/graphql",
  usersURI: "$deployedURI:8080/login",
);

AppConfig getConfig() {
  return localConfig;
}
