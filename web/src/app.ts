import express from 'express';
import * as bodyParser from 'body-parser';
import * as path from 'path';
import logger from 'morgan';

import * as indexRouter from './routes/index';
import * as filmsRouter from './routes/films';

const app: express.Application = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

// app setup
app.use(bodyParser.urlencoded({extended: true}));
app.use(logger('dev'));
app.use(express.json());
app.use(express.static(path.join(__dirname, 'public')));

// Custom routes
app.use('/', indexRouter.router);
app.use('/films', filmsRouter.router);

app.listen(3000);
