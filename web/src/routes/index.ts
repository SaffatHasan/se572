import { Router as expressRouter } from "express";
export const router = expressRouter();

router.get("/", function (req, res, next) {
  res.render("index", { title: "Web", currentPage: "index" });
});
