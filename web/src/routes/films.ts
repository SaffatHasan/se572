import { Router as expressRouter } from "express";
export const router = expressRouter();

router.get("/", function (req, res, next) {
  res.render("films", { title: "Films", currentPage: "films" });
});

router.get("/update/:filmID", function (req, res, next) {
  res.render("update_film", {
    title: "Update Films",
    filmID: req.params.filmID,
  });
});
