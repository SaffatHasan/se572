const LOGIN_SUCCESS_BTN = "#login-success";
const LOGIN_FAILURE_BTN = "#login-failure";
const SUCCESS_BTN = "#submission-success";
const FAILURE_BTN = "#submission-failure";
const TABLE_FAILURE_BTN = "#table-display-failure";
const SUCCESS_MSG = "Successful submission!";
const FAILURE_MSG = "Failed to validate ";

$.ajaxSetup({
  url: getConfig().filmsURI,
  type: "POST",
  contentType: "application/json",
});

// eslint-disable-next-line no-unused-vars
const API = (() => {
  let jwt;

  const login = () => {
    if (!$("#username").val()) {
      showAlert(LOGIN_FAILURE_BTN, "Username cannot be empty");
      return;
    }
    try {
      fetch(getConfig().usersURI, {
        method: "POST",
        body: JSON.stringify({
          username: $("#username").val(),
        }),
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json",
        },
      })
        .then((response) => response.json())
        .then((data) => {
          jwt = data.token;
          showAlert(LOGIN_SUCCESS_BTN, "Successfully logged in!");
        });
    } catch (error) {
      console.log(error);
    }
  };

  validators = [
    {
      name: "name",
      isFieldValid: () => validateFilmName(),
    },
    {
      name: "rating",
      isFieldValid: () => validateFilmRating(),
    },
  ];

  const createFilm = () => {
    for (const validator of validators) {
      if (!validator.isFieldValid()) {
        showAlert(FAILURE_BTN, FAILURE_MSG + validator.name);
        return;
      }
    }

    $.ajax({
      data: JSON.stringify({
        query: `mutation addFilm ($name: String!, $rating: Int!) {
          addFilm(name: $name, rating: $rating) {
            name
          }
        }`,
        variables: {
          name: $("input#name").val(),
          rating: parseInt($("select#rating").val()),
        },
      }),
      headers: {
        Authorization: jwt,
      },
      success: (response) => {
        if (response.errors) {
          showAlert(FAILURE_BTN, response.errors[0].message);
          return;
        }
        showAlert(SUCCESS_BTN, SUCCESS_MSG);
        $("input#name").val("");
      },
      error: (response) => {
        if (response.status === 401) {
          showAlert(FAILURE_BTN, "Unauthorized. Please login!");
        }
      },
    });
  };

  const updateFilm = () => {
    for (const validator of validators) {
      if (!validator.isFieldValid()) {
        showAlert(FAILURE_BTN, FAILURE_MSG + validator.name);
        return;
      }
    }

    $.ajax({
      data: JSON.stringify({
        query: `mutation updateFilm($_id: String!, $name: String!, $rating: Int!) {
          updateFilm(_id: $_id, name: $name, rating: $rating) {
            name
          }
        }`,
        variables: {
          _id: $("input#id").val(),
          name: $("input#name").val(),
          rating: parseInt($("select#rating").val()),
        },
      }),
      headers: {
        Authorization: jwt,
      },
      success: (response) => {
        if (response.errors) {
          showAlert(FAILURE_BTN, response.errors[0].message);
          return;
        }
        showAlert(SUCCESS_BTN, SUCCESS_MSG);
      },
      error: (response) => {
        if (response.status === 401) {
          showAlert(FAILURE_BTN, "Unauthorized. Please login!");
        }
      },
    });
  };

  function validateFilmName() {
    return $("input#name").val() != "";
  }

  function validateFilmRating() {
    const userInputRating = $("select#rating").val();
    const validInput = ["1", "2", "3", "4", "5"];
    return validInput.indexOf(userInputRating) !== -1;
  }

  function showAlert(buttonSelector, message) {
    $(buttonSelector).slideDown();
    $(buttonSelector).text(message);
    setTimeout(() => {
      $(buttonSelector).slideUp();
    }, 3000);
  }

  const getFilms = (dataManipulationFunction) => {
    $.ajax({
      data: JSON.stringify({
        query: `query {
          films {
            _id
            name
            rating
          }
        }`,
      }),
      headers: {
        Authorization: jwt,
      },
      success: (result) => dataManipulationFunction(result.data.films),
      error: (response) => {
        if (response.status === 401) {
          showAlert(TABLE_FAILURE_BTN, "Unauthorized. Please login!");
        }
      },
    });
  };

  return {
    createFilm,
    getFilms,
    login,
    updateFilm,
  };
})();
