/**
 * films.js
 */

$(document).ready(() => {
  API.getFilms((result) => populateDefaults(result));
});

function populateDefaults(films) {
  for (const film of films) {
    if (film._id == $("input#id").val()) {
      setName(film);
      setRating(film);
    }
  }
}

function setName(film) {
  $("input#name").val(film.name);
}

function setRating(film) {
  $("input#rating").val(film.rating);
}
