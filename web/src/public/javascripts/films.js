/**
 * films.js
 */

let table;

$(document).ready(() => {
  setupTabulator();
  updateTable();
});

function setupTabulator() {
  table = new Tabulator("#film-table", {
    layout: "fitColumns",
    columns: [
      { title: "Name", field: "name" },
      { title: "Rating", field: "rating", formatter: "star" },
      {
        title: "Edit",
        field: "_id",
        formatter: "link",
        formatterParams: { urlPrefix: "/films/update/", label: "edit" },
      },
    ],
  });
}

function updateTable() {
  API.getFilms((result) => table.setData(result));
}
