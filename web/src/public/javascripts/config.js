class AppConfig {
  constructor(usersURI, filmsURI) {
    this.usersURI = usersURI;
    this.filmsURI = filmsURI;
  }
}

const deployedURI = "http://192.168.1.152";

// eslint-disable-next-line no-unused-vars
localConfig = new AppConfig(
  `${deployedURI}:3002/login`,
  `${deployedURI}:3001/graphql`
);

// eslint-disable-next-line no-unused-vars
dockerConfig = new AppConfig(
  `${deployedURI}:8080/login`,
  `${deployedURI}:8080/graphql`
);

// eslint-disable-next-line no-unused-vars
function getConfig() {
  return localConfig;
}
