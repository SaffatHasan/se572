import jwt from "jsonwebtoken";
import * as config from "./config";

export const getUser = (authHeader: string): User => {
  if (!authHeader) {
    return <User>{
      isLoggedIn: false,
    };
  }
  const token = mapHeaderToToken(authHeader);
  const secretToken = config.APPLICATION_SECRET;

  const decodedToken = jwt.verify(token, secretToken, {
    complete: true,
  }) as JWT;

  const user: TokenData = decodedToken.payload;
  return { name: user.username, isLoggedIn: true };
};

function mapHeaderToToken(authHeader: string): string {
  if (isJson(authHeader)) {
    return JSON.parse(authHeader).token;
  }
  return authHeader;
}

function isJson(str) {
  try {
    JSON.parse(str);
  } catch (e) {
    return false;
  }
  return true;
}

export interface User {
  name: string;
  isLoggedIn: boolean;
}

export interface TokenData {
  username: string;
}

export interface JWT {
  payload: any;
}
