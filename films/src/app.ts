/**
 * Sample GraphQL service
 * Inspired by https://www.youtube.com/watch?v=ZQL7tL2S0oQ
 */
import * as auth from "./auth";
import * as bodyParser from "body-parser-graphql";
import connect from "./db";
import cors from "cors";
import express from "express";
import expressGraphQL from "express-graphql";
import schema from "./gql/schema";

const app: express.Application = express();
const port = 3001;

app.use(bodyParser.graphql());
app.use(cors());

app.use(
  "/graphql",
  expressGraphQL(async (req, res, graphQLParams) => {
    return {
      schema: schema,
      context: {
        user: auth.getUser(req.headers.authorization),
        db: await connect(),
      },
      graphiql: true,
    };
  })
);

app.listen(port, () => {
  console.log(`Node Graphql API started on ${port}!`);
});
