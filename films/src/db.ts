import { MongoClient } from "mongodb";
import { DB_URI, DB_NAME } from "./config";

let db;
export default async function () {
  if (db) {
    return db;
  }
  try {
    const mongoClient = await MongoClient.connect(DB_URI, {
      useUnifiedTopology: true,
    });
    console.log("Successfully connected to mongodb");
    db = mongoClient.db(DB_NAME);
  } catch (error) {
    console.error("Failed to connect to mongodb");
    return null;
  }
  return db;
}
