export let DB_URI = "mongodb://localhost:27017";
export let DB_NAME = "FilmsDB";
export let APPLICATION_SECRET = "secret";

if (process.env.MONGO_DB_URI) {
  DB_URI = process.env.MONGO_DB_URI;
}

if (process.env.MONGO_DB_NAME) {
  DB_NAME = process.env.MONGO_DB_NAME;
}

if (process.env.APPLICATION_SECRET) {
  APPLICATION_SECRET = process.env.APPLICATION_SECRET;
}
