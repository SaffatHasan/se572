import {
  GraphQLObjectType,
  GraphQLString,
  GraphQLInt,
  GraphQLNonNull,
} from "graphql";
import { FilmType } from "./types";
import { ObjectID } from "mongodb";

export const RootMutationType = new GraphQLObjectType({
  name: "Mutation",
  description: "Root Mutation",
  fields: () => ({
    addFilm: {
      type: FilmType,
      description: "Add a default film",
      args: {
        name: { type: new GraphQLNonNull(GraphQLString) },
        rating: { type: new GraphQLNonNull(GraphQLInt) },
      },
      resolve: async (parent, args, { db, user }) => {
        if (!user.isLoggedIn) {
          throw new Error("You must log in!");
        }
        if (!isRatingValid(args.rating)) {
          throw invalidRatingError(args.rating);
        }
        const film = {
          name: args.name,
          rating: args.rating,
        };
        const result = await db.collection("films").insertOne(film);
        return result.ops[0];
      },
    },
    updateFilm: {
      type: FilmType,
      description: "Update a film",
      args: {
        _id: { type: new GraphQLNonNull(GraphQLString) },
        name: { type: new GraphQLNonNull(GraphQLString) },
        rating: { type: new GraphQLNonNull(GraphQLInt) },
      },
      resolve: async (_, args, { db, user }) => {
        if (!user.isLoggedIn) {
          throw new Error("You must log in!");
        }
        if (!isRatingValid(args.rating)) {
          throw invalidRatingError(args.rating);
        }
        const film = {
          _id: args._id,
          name: args.name,
          rating: args.rating,
        };
        await db.collection("films").updateOne(
          { _id: new ObjectID(film._id) },
          {
            $set: {
              name: args.name,
              rating: args.rating,
            },
          },
          { upsert: false }
        );
        return film;
      },
    },
    clear: {
      type: GraphQLInt,
      resolve: async (parent, args, { db }) => {
        const result = await db.collection("films").deleteMany({});
        return result.deletedCount;
      },
    },
  }),
});

function isRatingValid(rating: any): boolean {
  const VALID_RATINGS = [1, 2, 3, 4, 5];
  return VALID_RATINGS.indexOf(rating) !== -1;
}

function invalidRatingError(rating: any): Error {
  return new Error(
    "Invalid rating received [type:" + typeof rating + "](" + rating + ") "
  );
}
