import { GraphQLObjectType, GraphQLList } from "graphql";
import { FilmType } from "./types";

export const RootQueryType = new GraphQLObjectType({
  name: "Query",
  description: "Root Query",
  fields: () => ({
    films: {
      type: new GraphQLList(FilmType),
      description: "List of all films",
      resolve: async (parent, args, { db }) => {
        return await db.collection("films").find().toArray();
      },
    },
  }),
});
