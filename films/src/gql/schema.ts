import { GraphQLSchema } from "graphql";
import { RootMutationType } from "./mutations";
import { RootQueryType } from "./queries";

export default new GraphQLSchema({
  query: RootQueryType,
  mutation: RootMutationType,
});
