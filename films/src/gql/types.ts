import {
  GraphQLObjectType,
  GraphQLString,
  GraphQLInt,
  GraphQLNonNull,
} from "graphql";

export const FilmType = new GraphQLObjectType({
  name: "Film",
  description: "this represents a film",
  fields: () => ({
    _id: { type: GraphQLString },
    name: { type: new GraphQLNonNull(GraphQLString) },
    rating: { type: new GraphQLNonNull(GraphQLInt) },
  }),
});
