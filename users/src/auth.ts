import * as config from "./config";
import jwt from "jsonwebtoken";

export const generateToken = (req, res) => {
  const user = req.body.username;
  if (user === undefined) {
    res.status(400);
    res.send("Bad request: Missing Username");
    return;
  }
  const data: TokenData = { username: user };
  const secretToken = config.APPLICATION_SECRET;
  const token = jwt.sign(data, secretToken);
  res.json({ token });
};

export const getUser = (token: string): User => {
  if (!token) {
    return <User>{
      isLoggedIn: false,
    };
  }
  const payload = jwt.decode(token, { complete: true });
  const user: TokenData = payload["user"];
  return { name: user.username, isLoggedIn: true };
};

export interface User {
  name: string;
  isLoggedIn: boolean;
}

export interface TokenData {
  username: string;
}
