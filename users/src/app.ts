/**
 * Sample GraphQL service
 * Inspired by https://www.youtube.com/watch?v=ZQL7tL2S0oQ
 */
import * as auth from "./auth";
import * as bodyParser from "body-parser";
import cors from "cors";
import express from "express";

const app: express.Application = express();
const port = 3002;

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(cors());

// auth middleware
app.post("/login", auth.generateToken);

app.listen(port, () => {
  console.log(`Users started on ${port}!`);
});
