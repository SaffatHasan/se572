export let PORT = 3002;
export let APPLICATION_SECRET = "secret";

if (process.env.USERS_PORT) {
  PORT = parseInt(process.env.USERS_PORT);
}

if (process.env.APPLICATION_SECRET) {
  APPLICATION_SECRET = process.env.APPLICATION_SECRET;
}
