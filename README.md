# SE572 Microservice Proof Of Concept

This project represents this culmination of the work performed in SE572. This project explores the usage of interesting architectures and frameworks such as microservice architecture, Flutter, and, GraphQL.

## Full Application Walkthrough Video

<div align="center">

[![Full Application Walkthrough](https://img.youtube.com/vi/SeNVdpBxxcU/0.jpg)](https://www.youtube.com/watch?v=SeNVdpBxxcU)

</div>

## Local Setup

This section will describe how to setup and run the application.

### Pre-requisites

1. Visual Studio Code
1. npm (films, users, web)
1. NodeJS (films, users, web)
1. Flutter (flutter_app)
1. Docker (database)

### Config

1. Update `flutter_app`'s [config](flutter_app/lib/config.dart) and `web`'s [config](web/src/public/javascripts/config.js)
   - Update `deployedURI` to either your computer IPv4 address, `localhost`, or, `127.0.0.1`
1. Set `getConfig()`'s return value to `localConfig` or `dockerConfig`

## Running

1. Start the database with `docker run -d -p 27017:27017 --name mymongo mongo`
1. Open Visual Studio Code
1. Run > Start Debugging (Or F5)
   - Be sure to select `Local All (except db!)`
   - OR run a subset of these items

## Design

<div align="center">
<img src="docs/component.png" align="center" width="308" alt="Project icon">

> **_NOTE:_** `client` is one of `flutter_app` OR `web`

</div>
